﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cure_Clinic.Models;

namespace Cure_Clinic
{
    public static class LogedUser
    {
        public static bool IsLoged()
        {
            CureClinicEntities db = new CureClinicEntities();
            bool isvalid = false;
            try
            {
                String email = HttpContext.Current.Request.Cookies["auth"].Value.Split('|')[0],
                               pass = HttpContext.Current.Request.Cookies["auth"].Value.Split('|')[1];
                var user = db.Users.Where(u => (u.UserName == email && u.Password == pass));
                if (user != null)
                {
                    isvalid = true;
                }
            }
            catch
            {
            }
            return isvalid;
        }
        public static Cure_Clinic.Models.User CurrentUser()
        {
            CureClinicEntities db = new CureClinicEntities();
            string username = HttpContext.Current.Request.Cookies["auth"].Value.Split('|')[0],
                pass = HttpContext.Current.Request.Cookies["auth"].Value.Split('|')[1];
            var usr = db.Users.Where(u => u.UserName == username && u.Password == pass).FirstOrDefault();

            return usr;
        }
    }
}