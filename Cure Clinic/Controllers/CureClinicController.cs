﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cure_Clinic.Models;
using System.Web.Script.Serialization;

namespace Cure_Clinic.Controllers
{
    public class CureClinicController : Controller
    {
        CureClinicEntities db = new CureClinicEntities();

        #region Home
        public ActionResult Index()
        {
            var home = db.Homes.ToList();
            return View(home);
        }
        #endregion

        #region About
        public ActionResult About(int Id)
        {
            var doctors = db.Abouts.Where(d => d.Id == Id).FirstOrDefault();
            return View(doctors);
        }

        [HttpPost]
        public ActionResult About(FormCollection collection, HttpPostedFileBase DoctorImage)
        {
            int id = int.Parse(collection["Id"]);

            HttpPostedFileBase Image = Request.Files["DoctorPic"];

            About doctor = db.Abouts.Where(h => h.Id == id).FirstOrDefault();

            string Text = collection["Definition"];

            return View();
        }

        public ActionResult AboutAllDoctors()
        {
            var doctors = db.Abouts.ToList();
            return View(doctors);
        }
        #endregion

        #region Contact
        public ActionResult Contact()
        {
            var contact = db.Locations.FirstOrDefault();
            return View(contact);
        }

        [HttpPost]
        public ActionResult Contact(FormCollection collection)
        {
            string Name = collection["name"];
            string EmailAddress = collection["email"];
            string msg = collection["message"];

            Contact contactN = new Contact();
            contactN.Name = Name;
            contactN.Email = EmailAddress;
            contactN.Message = msg;
            contactN.Date = DateTime.Now;
            contactN.IsSeen = false;

            db.Contacts.Add(contactN);
            if (db.SaveChanges() > 0)
                return Json(new { success = true, message = "Sending Success" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Sending Falied" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Service
        public ActionResult Services()
        {
            var services = db.MainServices.ToList();
            return View(services);
        }

        public ActionResult SubServices(int id)
        {
            var query = (from m in db.SubServices
                         where m.MainServiceId == id
                         select m.MainService.Title).FirstOrDefault();
            ViewBag.MainTitle = query;

            var results = db.SubServices.Where(s => s.MainServiceId == id).ToList();
            return View(results);
        }

        public ActionResult Details(int id)
        {
            var details = db.SubServices.Where(r => r.Id == id).FirstOrDefault();
            return View(details);
        }

        [HttpPost]
        public ActionResult Details(FormCollection collection, HttpPostedFileBase HospitalImage)
        {
            int id = int.Parse(collection["Id"]);

            HttpPostedFileBase Image = Request.Files["Image"];

            SubService hospital = db.SubServices.Where(h => h.Id == id).FirstOrDefault();

            string Text = collection["text"];

            return View();
        }
        #endregion

        #region Blog
        public ActionResult Blog()
        {
            var blog = db.Blogs.OrderByDescending(M => M.PostDate).ToList().Take(6);
            return View(blog);
        }

        public ActionResult PostDetails(int id)
        {
            var details = db.Blogs.Where(b => b.Id == id).FirstOrDefault();
            return View(details);
        }

        [HttpPost]
        public ActionResult PostDetails(FormCollection collection)
        {
            int PostId = Convert.ToInt32(collection["Id"]);
            string Comment = collection["Comment"];

            Comment comment = db.Comments.Where(C => C.Comment1 == Comment).FirstOrDefault();

            Comment commentN = new Comment();
            commentN.Comment1 = Comment;
            commentN.BlogId = PostId;
            commentN.CommentDate = DateTime.Now;

            db.Comments.Add(commentN);
            if (db.SaveChanges() > 0)
                return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "The comment was not saved" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}