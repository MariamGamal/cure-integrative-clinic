﻿using Cure_Clinic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Web.Mvc;
using Cure_Clinic;
using System.Data.Entity;

namespace DashBoard_CureClinic.Controllers
{
    public class adminController : Controller
    {
        //database entity 
        CureClinicEntities db = new CureClinicEntities();

        // GET: admin
        public ActionResult Index()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View();
            else
                return RedirectToAction("Login");
        }

        public ActionResult IndexAdmin()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                return View();
            else
                return RedirectToAction("Login");
        }

        #region Home
        public ActionResult HomeEditing()
        {
            return View();
        }

        public ActionResult Slider()
        {
            var slider = db.Homes.FirstOrDefault();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                return View(slider);
            else
                return RedirectToAction("Login");
           
        }

        [HttpPost]
        public ActionResult Slider(FormCollection collection)
        {
            int HomeId = Convert.ToInt32(collection["HomeId"]);
            string SliderText1 = collection["SliderText1"];
            string SliderText2 = collection["SliderText2"];
            string SliderText3 = collection["SliderText3"];
            HttpPostedFileBase Image1 = Request.Files["Image1"];
            HttpPostedFileBase Image2 = Request.Files["Image2"];
            HttpPostedFileBase Image3 = Request.Files["Image3"];

            Home homeList = db.Homes.Where(s => s.Id == HomeId).FirstOrDefault();
            if(homeList != null)
            {
                if (homeList.SlideText1 != SliderText1 || homeList.SlideText2 != SliderText2 || homeList.SlideText3 != SliderText3)
                {
                    Home OldHome = db.Homes.Where(b => b.SlideText1 == SliderText1 && b.SlideText2 == SliderText2 
                                                    && b.SlideText3 == SliderText3).FirstOrDefault();
                    if(OldHome == null)
                    {
                        homeList.SlideText1 = SliderText1;
                        homeList.SlideText2 = SliderText2;
                        homeList.SlideText3 = SliderText3;
                       
                        if (Image1 != null && Image1.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image1.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                            Image1.SaveAs(path);

                            homeList.SlideImg1 = "/Upload/HomeImage/" + rondom;
                        }
                        if (Image2 != null && Image2.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image2.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                            Image2.SaveAs(path);
                            
                            homeList.SlideImg2 = "/Upload/HomeImage/" + rondom;
                        }
                        if (Image3 != null && Image3.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image3.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                            Image3.SaveAs(path);
                            
                            homeList.SlideImg3 = "/Upload/HomeImage/" + rondom;
                        }
                        if (db.SaveChanges() > 0)
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "The Text was not Edited" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This Text was previously saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Boxes()
        {
            var homebox = db.HomeBoxes.FirstOrDefault();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                return View(homebox);
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult Boxes(FormCollection collection)
        {
            int HomeBoxId = Convert.ToInt32(collection["HomeBoxId"]);
            string Text1 = collection["Text1"];
            string Text2 = collection["Text2"];
            string Text3 = collection["Text3"];
            HttpPostedFileBase Image1 = Request.Files["Image1"];
            HttpPostedFileBase Image2 = Request.Files["Image2"];
            HttpPostedFileBase Image3 = Request.Files["Image3"];
            HomeBox homeList = db.HomeBoxes.Where(s => s.Id == HomeBoxId).FirstOrDefault();
            if (homeList != null)
            {
                if (homeList.Text1 != Text1 || homeList.Text2 != Text2 || homeList.Text3 != Text3)
                {
                    HomeBox OldHome = db.HomeBoxes.Where(b => b.Text1 == Text1 && b.Text2 == Text2
                                                    && b.Text3 == Text3).FirstOrDefault();
                    if (OldHome == null)
                    {
                        homeList.Text1 = Text1;
                        homeList.Text2 = Text2;
                        homeList.Text3 = Text3;

                        if (Image1 != null && Image1.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image1.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/HomeBoxImage"), Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/HomeBoxImage"));
                            Image1.SaveAs(path);

                            homeList.Img1 = "/Upload/HomeBoxImage/" + rondom;
                        }
                        if (Image2 != null && Image2.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image2.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/HomeBoxImage"), Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/HomeBoxImage"));
                            Image2.SaveAs(path);

                            homeList.Img2 = "/Upload/HomeBoxImage/" + rondom;
                        }
                        if (Image3 != null && Image3.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image3.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/HomeBoxImage"), Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/HomeBoxImage"));
                            Image3.SaveAs(path);

                            homeList.Img3 = "/Upload/HomeBoxImage/" + rondom;
                        }
                        if (db.SaveChanges() > 0)
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "The Text was not Edited" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This Text was previously saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Gallery()
        {
            var home = db.Homes.FirstOrDefault();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                return View(home);
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult Gallery(FormCollection collection)
        {
            int HomeId = Convert.ToInt32(collection["HomeId"]);
            HttpPostedFileBase Image1 = Request.Files["Image1"];
            HttpPostedFileBase Image2 = Request.Files["Image2"];
            HttpPostedFileBase Image3 = Request.Files["Image3"];
            HttpPostedFileBase Image4 = Request.Files["Image4"];
            HttpPostedFileBase Image5 = Request.Files["Image5"];
            HttpPostedFileBase Image6 = Request.Files["Image6"];


            Home home = db.Homes.Where(s => s.Id == HomeId).FirstOrDefault();

            if(home != null)
            {
                if (Image1 != null && Image1.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image1.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                    Image1.SaveAs(path);

                    home.GPhoto1 = "/Upload/HomeImage/" + rondom;
                }
                if (Image2 != null && Image2.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image2.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                    Image2.SaveAs(path);

                    home.GPhoto2 = "/Upload/HomeImage/" + rondom;
                }
                if (Image3 != null && Image3.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image3.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                    Image3.SaveAs(path);

                    home.GPhoto3 = "/Upload/HomeImage/" + rondom;
                }
                if (Image4 != null && Image4.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image4.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                    Image4.SaveAs(path);

                    home.GPhoto4 = "/Upload/HomeImage/" + rondom;
                }
                if (Image5 != null && Image5.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image5.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                    Image5.SaveAs(path);

                    home.GPhoto5 = "/Upload/HomeImage/" + rondom;
                }
                if (Image6 != null && Image6.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image6.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/HomeImage"), Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/HomeImage"));
                    Image6.SaveAs(path);

                    home.GPhoto6 = "/Upload/HomeImage/" + rondom;
                }
                if (db.SaveChanges() > 0)
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = " Was not Edited" }, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }
         
        
        #endregion

        #region Login & Logout
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            bool userValid = db.Users.Any(user => user.UserName == username && user.Password == password);
            if (userValid)
            {
                var user = db.Users.Where(u => u.UserName == username && u.Password == password).FirstOrDefault();
                HttpCookie auth = new HttpCookie("auth", Request.Form["username"] + "|" + Request.Form["password"]);
                auth.Expires.AddDays(30);
                Response.Cookies.Add(auth);
                if (user.UserRole.RoleName == "Admin")
                    return RedirectToAction("IndexAdmin");
                return RedirectToAction("/Index");
            }
            else
            {
                ViewBag.Message = "Username or Password Error ..";
                return View();
            }
        }

        public ActionResult Logout()
        {
            Response.Cookies["auth"].Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Login");
        }
        #endregion

        #region About
        public ActionResult About()
        {
            int userId = LogedUser.CurrentUser().Id;
            var doctors = db.Abouts.Where(d => d.UserId == userId).FirstOrDefault();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View(doctors);
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult About(FormCollection collection)
        {
            int id = int.Parse(collection["AboutId"]);
            About about = db.Abouts.Where(h => h.Id == id).FirstOrDefault();

            string Text = collection["Text"];
            HttpPostedFileBase Image = Request.Files["Image"];

            if (about != null)
            {
                if (about.Definition != Text)
                {
                    About OldAbout = db.Abouts.Where(s => s.Definition == Text).FirstOrDefault();

                    if (OldAbout == null)
                    {
                        about.Definition = Text;

                        if (Image != null && Image.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/AboutImage"),
                                             Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/AboutImage"));
                            Image.SaveAs(path);
                            about.DoctorPic = "/Upload/AboutImage/" + rondom;
                        }

                        if (db.SaveChanges() > 0)
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "The Text was not saved" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This Text does not change" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CureClinicStaff()
        {
            
            int userId = LogedUser.CurrentUser().Id;
            var doctors = db.Abouts.Where(d => d.UserId == userId).FirstOrDefault();
            if (doctors != null)
            {
                if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                    return View(doctors);
                else
                    return RedirectToAction("Login");
            }
            else
            {
                return RedirectToAction("DoctorAbout");
            }
        }

        public ActionResult DoctorAbout()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View();
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult DoctorAbout(FormCollection collection)
        {
            int userId = LogedUser.CurrentUser().Id;
            string Name = collection["Name"];
            string Job = collection["Job"];
            string Definition = collection["Definition"];
            HttpPostedFileBase Image = Request.Files["Image"];
            About doctor = db.Abouts.Where(s => s.DoctorName == Name).FirstOrDefault();
            if(doctor == null)
            {
                About newdoctor = new About();
                newdoctor.DoctorName = Name;
                newdoctor.Definition = Definition;
                newdoctor.Job = Job;
                newdoctor.UserId = userId;
                if (Image != null && Image.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/AboutImage"),Path.GetFileName(rondom));

                    Directory.CreateDirectory(Server.MapPath("/Upload/AboutImage"));
                    Image.SaveAs(path);
                    newdoctor.DoctorPic = "/Upload/AboutImage/" + rondom;

                }

                db.Abouts.Add(newdoctor);
                if (db.SaveChanges() > 0)
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "The Doctor was not saved" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "The Doctor was previously saved" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
  
        #region Blog
        public ActionResult Blog()
        {
            int userId = LogedUser.CurrentUser().Id;
            var posts = db.Blogs.Where(d => d.UserId == userId).ToList();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View(posts);
            else
                return RedirectToAction("Login");
        }

        public ActionResult AddBlog()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View();
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult AddBlog(FormCollection collection)
        {
            int userId = LogedUser.CurrentUser().Id;
            string Title = collection["Title"];
            string Text = collection["Text"];

            HttpPostedFileBase Image = Request.Files["Image"];

            Blog blog = db.Blogs.Where(s => s.Title == Title).FirstOrDefault();

            if (blog == null)
            {
                Blog newblog = new Blog();
                newblog.Title = Title;
                newblog.PostText = Text;
                newblog.PostDate = DateTime.Now;
                newblog.UserId = userId;

                if (Image != null && Image.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/BlogImage"),
                                     Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/BlogImage"));
                    Image.SaveAs(path);
                    newblog.PostImg = "/Upload/BlogImage/" + rondom;
                }

                db.Blogs.Add(newblog);
                if (db.SaveChanges() > 0)
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "The Post was not saved" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "This Post was previously saved" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditBlog(int id)
        {
            var blog = db.Blogs.Where(s => s.Id == id).FirstOrDefault();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View(blog);
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult EditBlog(FormCollection collection)
        {
            int BlogId = int.Parse(collection["BlogId"]);
           
            Blog blog = db.Blogs.Where(b => b.Id == BlogId).FirstOrDefault();

            string Title = collection["Title"];
            string Text = collection["Text"];

            HttpPostedFileBase Image = Request.Files["Image"];

            if (blog != null)
            {
                if (blog.Title != Title || blog.PostText != Text )
                {
                    Blog OldBlog = db.Blogs.Where(b => b.Title == Title && b.PostText == Text).FirstOrDefault();

                    if (OldBlog == null)
                    {
                        blog.Title = Title;
                        blog.PostText = Text;

                        if (Image != null && Image.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/BlogImage"),
                                             Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/BlogImage"));
                            Image.SaveAs(path);
                            blog.PostImg = "/Upload/BlogImage/" + rondom;
                        }

                        if (db.SaveChanges() > 0)
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "The Post was not Edited" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This Post was previously saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PostComments(int id)
        {
            var posts = db.Blogs.Where(p => p.Id == id).FirstOrDefault();
            var comment = db.Comments.Where(d => d.BlogId == posts.Id).ToList();
            if (comment != null)
            {
                if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                    return View(comment);
                else
                    return RedirectToAction("Login");
            }
            else
            {
                return RedirectToAction("PostComments");
            }
        }

        public ActionResult DeleteComment(string selectedItems)
        {
            string[] Items = selectedItems.Split(new char[] { ',' });
            List<int> AllItems = new List<int>();
            foreach (var item in Items)
            {
                var dbrec = db.Comments.Find(int.Parse(item));
                db.Comments.Remove(dbrec);
            }
            db.SaveChanges();

            return RedirectToAction("Blog");
        }


        public ActionResult DeleteBlog(int id)
        {
            Blog blog = db.Blogs.FirstOrDefault(s => s.Id.Equals(id));
            Comment comment = db.Comments.Where(u => u.BlogId == blog.Id).FirstOrDefault();

            if (blog != null && comment != null)
            {
                db.Comments.Remove(comment);
                db.Blogs.Remove(blog);
                db.SaveChanges();
            }
            else if (blog != null && comment == null)
            {
                db.Blogs.Remove(blog);
                db.SaveChanges();
            }

            return RedirectToAction("Blog");
        }
        #endregion

        #region MainService
        public ActionResult Services()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View();
            else
                return RedirectToAction("Login");
        }

        public ActionResult GetMainService()
        {
            var mainservices = db.MainServices.ToList();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View(mainservices);
            else
                return RedirectToAction("Login");
        }

        public ActionResult AddMainService()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View();
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult AddMainService(FormCollection collection)
        {
            string Title = collection["Title"];
            MainService main = db.MainServices.Where(m => m.Title == Title).FirstOrDefault();
            if (main == null)
            {
                MainService mainN = new MainService();
                mainN.Title = Title;
                db.MainServices.Add(mainN);
                if (db.SaveChanges() > 0)
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "The service was not saved" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "This service was previously saved" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditMainService(int id)
        {
            var main = db.MainServices.Where(m => m.Id == id).FirstOrDefault();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View(main);
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult EditMainService(FormCollection collection)
        {
            int MainId = Convert.ToInt32(collection["Id"]);
            MainService main = db.MainServices.Where(c => c.Id == MainId).FirstOrDefault();

            string Title = collection["Title"];
            if (main != null)
            {
                if (main.Title != Title)
                {
                    MainService OldMain = db.MainServices.Where(c => c.Title == Title).FirstOrDefault();
                    if (OldMain == null)
                    {
                        main.Title = Title;
                        if (db.SaveChanges() > 0)
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "The service was not Edited " }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This service was previously saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteMainService(int id)
        {
            MainService main = db.MainServices.FirstOrDefault(m => m.Id.Equals(id));
            try
            {
                if (main != null)
                {
                    db.MainServices.Remove(main);
                    db.SaveChanges();
                }
            }
            catch
            {
                TempData["DeleteServiceError"] = "You must first delete the sub services of this main service ...";

                return RedirectToAction("GetMainService", main);
            }

            return RedirectToAction("GetMainService");

        }
        #endregion

        #region Subservice
        public ActionResult GetServices()
        {
            List<SubService> allSubServices = getAllSubServices();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View(allSubServices);
            else
                return RedirectToAction("Login");
        }

        public List<SubService> getAllSubServices()
        {
            var query = (from s in db.SubServices
                         from m in db.MainServices
                         where s.MainServiceId == m.Id
                         select s).ToList();
            return query;
        }

        public ActionResult AddService()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View();
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult AddService(FormCollection collection)
        {
            string Title = collection["Title"];
            string Text = collection["Text"];
            int MainServiceId = int.Parse(collection["Id"]);

            HttpPostedFileBase Image = Request.Files["Image"];

            SubService sub = db.SubServices.Where(s => s.Title == Title).FirstOrDefault();

            if (sub == null)
            {
                SubService newservice = new SubService();
                newservice.Title = Title;
                newservice.Text = Text;
                newservice.MainServiceId = MainServiceId;

                if (Image != null && Image.ContentLength > 0)
                {
                    var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image.FileName);
                    string path = Path.Combine(Server.MapPath("/Upload/ServicesImage"),
                                     Path.GetFileName(rondom));
                    Directory.CreateDirectory(Server.MapPath("/Upload/ServicesImage"));
                    Image.SaveAs(path);
                    newservice.Image = "/Upload/ServicesImage/" + rondom;

                }

                db.SubServices.Add(newservice);
                if (db.SaveChanges() > 0)
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "The service was not saved" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "This service was previously saved" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditService(int id)
        {
            var sub = db.SubServices.Where(s => s.Id == id).FirstOrDefault();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                return View(sub);
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult EditService(FormCollection collection)
        {
            int SubId = int.Parse(collection["SubId"]);
            int MainServiceId = int.Parse(collection["Id"]);

            SubService sub = db.SubServices.Where(s => s.Id == SubId).FirstOrDefault();

            string Title = collection["Title"];
            string Text = collection["Text"];

            HttpPostedFileBase Image = Request.Files["Image"];

            if (sub != null)
            {
                if (sub.Title != Title || sub.Text != Text || sub.MainServiceId != MainServiceId)
                {
                    SubService OldSub = db.SubServices.Where(s => s.Title == Title && s.Text == Text).FirstOrDefault();

                    if (OldSub == null)
                    {
                        sub.Title = Title;
                        sub.Text = Text;
                        sub.MainServiceId = MainServiceId;

                        if (Image != null && Image.ContentLength > 0)
                        {
                            var rondom = Path.GetRandomFileName().Replace(".", "") + Path.GetExtension(Image.FileName);
                            string path = Path.Combine(Server.MapPath("/Upload/ServicesImage"),
                                             Path.GetFileName(rondom));
                            Directory.CreateDirectory(Server.MapPath("/Upload/ServicesImage"));
                            Image.SaveAs(path);
                            sub.Image = "/Upload/ServicesImage/" + rondom;
                        }

                        if (db.SaveChanges() > 0)
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "The service was not Edited" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This service was previously saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteService(int id)
        {
            SubService sub = db.SubServices.FirstOrDefault(s => s.Id.Equals(id));
            try
            {
                if (sub != null)
                {
                    db.SubServices.Remove(sub);
                    db.SaveChanges();
                }
            }
            catch
            {
                TempData["DeleteServiceError"] = "You must first delete any service depend on  this service ...";

                return RedirectToAction("GetServices", sub);
            }

            return RedirectToAction("GetServices");
        }
        #endregion

        #region Contact
        public ActionResult Contact()
        {
            List<Contact> allmessages = getAllContactMessages();
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                return View(allmessages);
            else
                return RedirectToAction("Login");
        }

        public List<Contact> getAllContactMessages()
        {
            var query = (from c in db.Contacts
                         orderby c.Id descending
                         select c).ToList();
            return query;
        }

        public ActionResult ContactMessageDetails(int id)
        {
            Contact Message = db.Contacts.Where(r => r.Id == id).FirstOrDefault();
            Message.IsSeen = true;
            db.SaveChanges();
            return View(Message);
        }

        public ActionResult DeleteMessage(int id)
        {
            Contact contact = db.Contacts.FirstOrDefault(s => s.Id.Equals(id));

            if (contact != null)
            {
                db.Contacts.Remove(contact);
                db.SaveChanges();
            }

            return RedirectToAction("Contact");
        }
        #endregion

        #region Location
        public ActionResult Location()
        {
            var contact = db.Locations.FirstOrDefault();
            if (contact != null)
            {
                if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                    return View(contact);
                else
                    return RedirectToAction("Login");
            }
            else
            {
                return RedirectToAction("AddLocation");
            }
        }

        [HttpPost]
        public ActionResult Location(FormCollection collection)
        {
            int LocId = Convert.ToInt32(collection["Id"]);
            Location loc = db.Locations.Where(c => c.Id == LocId).FirstOrDefault();

            string Address = collection["Address"];
            string Phone = collection["Phone"];
            string Email = collection["Email"];

            if (loc != null)
            {
                if (loc.Address != Address || loc.Phone != Phone || loc.Email != Email)
                {
                    Location OldLoc = db.Locations.Where(c => c.Address == Address && c.Phone == Phone && c.Email == Email).FirstOrDefault();
                    if (OldLoc == null)
                    {
                        loc.Address = Address;
                        loc.Phone = Phone;
                        loc.Email = Email;
                        if (db.SaveChanges() > 0)
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "The Location was not Edited " }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This Location was previously saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddLocation()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                return View();
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult AddLocation(FormCollection collection)
        {
            string Address = collection["Address"];
            string Phone = collection["Phone"];
            string Email = collection["Email"];

            Location loc = db.Locations.Where(m => m.Address == Address).FirstOrDefault();
            if (loc == null)
            {
                Location locN = new Location();
                locN.Address = Address;
                locN.Phone = Phone;
                locN.Email = Email;
                
                db.Locations.Add(locN);
                if (db.SaveChanges() > 0)
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "This Location was not saved" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "This Location was previously saved" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Users
        public ActionResult AddDoctor()
        {
            if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin")
                return View();
            else
                return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult AddDoctor(FormCollection collection)
        {
            string Email = collection["Email"];
            string UserName = collection["UserName"];
            string Password = collection["Password"];
            int RoleId = int.Parse(collection["Id"]);

            User user = db.Users.Where(s => s.UserName == UserName).FirstOrDefault();
            if (user == null)
            {
                User userN = new User();
                userN.Email = Email;
                userN.UserName = UserName;
                userN.Password = Password;
                userN.RoleId = RoleId;

                db.Users.Add(userN);
                if (db.SaveChanges() > 0)
                {
                    var user1 = db.Users.FirstOrDefault(u => u.Email == Email);
                    if (user1 != null)
                    {
                        string toEmail = user1.Email;
                        string body = "This is your data which you can use to login Cure Integrative Clinic Site \n UserName : " + user1.UserName + "\n Password : " + user1.Password;
                        if (SendMail(toEmail, body))
                            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, message = "The data was not sent please try later" }, JsonRequestBehavior.AllowGet);
                    }

                    //return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, message = "The doctor was not saved" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "This doctor was previously saved" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ChangeUserData
        public ActionResult ChangeUserData()
        {
            try
            {
                int userId = LogedUser.CurrentUser().Id;
                var user = db.Users.Where(u => u.Id == userId).FirstOrDefault();
                if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "User")
                    return View(user);
                else
                    return RedirectToAction("Login");
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult ChangeUserData(FormCollection collection)
        {
            int UserId = Convert.ToInt32(collection["Id"]);
            User user = db.Users.Where(u => u.Id == UserId).FirstOrDefault();

            string Email = collection["Email"];
            string UserName = collection["UserName"];
            string Password = collection["Password"];
            string ConfirmPassword = collection["ConfirmPassword"];
            if (user != null)
            {
                if (user.UserName != UserName || user.Password != Password || user.Email == Email)
                {
                    User OldUser = db.Users.Where(u => u.UserName == UserName && u.Password == Password && u.Email == Email).FirstOrDefault();
                    if (OldUser == null)
                    {
                        user.Email = Email;
                        user.UserName = UserName;
                        user.Password = Password;
                        ConfirmPassword = Password;

                        if (db.SaveChanges() > 0)
                        {
                            return Json(new { success = true, message = "Successfully updated ..  :)" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, message = "User is not modified  " }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This user has already been saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAccount(FormCollection collection)
        {
            int userId = LogedUser.CurrentUser().Id;
            User user = db.Users.Where(u => u.Id == userId).FirstOrDefault();
            About about = db.Abouts.Where(a => a.UserId == userId).FirstOrDefault();
            var blog = db.Blogs.Where(b => b.UserId == userId).ToList();

            if (user != null && about != null && blog != null)
            {
                foreach (var b in blog)
                {
                    Comment comment = db.Comments.Where(u => u.BlogId == b.Id).FirstOrDefault();
                    if (comment != null)
                    {
                        db.Comments.Remove(comment);
                        db.Blogs.Remove(b);
                    }
                    else
                    {
                        db.Blogs.Remove(b);
                    }
                }
                db.Abouts.Remove(about);
                db.Users.Remove(user);
                db.SaveChanges();
            }
            else if (user != null && about != null && blog == null)
            {
                db.Abouts.Remove(about);
                db.Users.Remove(user);
                db.SaveChanges();
            }
            else if (user != null && about == null && blog != null)
            {
                foreach (var b in blog)
                {
                    Comment comment = db.Comments.Where(u => u.BlogId == b.Id).FirstOrDefault();
                    if (comment != null)
                    {
                        db.Comments.Remove(comment);
                        db.Blogs.Remove(b);
                    }
                    else
                    {
                        db.Blogs.Remove(b);
                    }
                }
                db.Users.Remove(user);
                db.SaveChanges();
            }
            else if (user != null && about == null && blog == null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
            }
            return RedirectToAction("Login");
        }
        #endregion

        #region ChangeAdminData
        public ActionResult ChangeAdminData()
        {
            try
            {
                int userId = LogedUser.CurrentUser().Id;
                var user = db.Users.Where(u => u.Id == userId).FirstOrDefault();
                if (LogedUser.IsLoged() && LogedUser.CurrentUser().UserRole.RoleName == "Admin" )
                    return View(user);
                else
                    return RedirectToAction("Login");
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult ChangeAdminData(FormCollection collection)
        {
            int UserId = Convert.ToInt32(collection["Id"]);
            User user = db.Users.Where(u => u.Id == UserId).FirstOrDefault();

            string Email = collection["Email"];
            string UserName = collection["UserName"];
            string Password = collection["Password"];
            string ConfirmPassword = collection["ConfirmPassword"];
            if (user != null)
            {
                if (user.UserName != UserName || user.Password != Password || user.Email == Email)
                {
                    User OldUser = db.Users.Where(u => u.UserName == UserName && u.Password == Password && u.Email == Email).FirstOrDefault();
                    if (OldUser == null)
                    {
                        user.Email = Email;
                        user.UserName = UserName;
                        user.Password = Password;
                        ConfirmPassword = Password;

                        if (db.SaveChanges() > 0)
                        {
                            return Json(new { success = true, message = "Successfully updated ..  :)" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, message = "Admin is not modified  " }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "This Admin has already been saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ForgotPassword
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(FormCollection collection)
        {
            string email = Request.Form["Email"];
            var user = db.Users.FirstOrDefault(u => u.Email == email);
            if (user != null)
            {
                string toEmail = user.Email;
                string body = "Your password has been restored and this is your data which you can use to login \n Email Address : " + user.Email + "\n UserName : " + user.UserName + "\n Password : " + user.Password;
                if (SendMail(toEmail, body))
                    return Json(new { success = true, message = "Your account password has been restored. Please check your email" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "The password was not sent please try later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool SendMail(string toEmail, string body)
        {
            string From = "cureintegrativeclinic@gmail.com";
            string To = toEmail;
            string Password = "cureclinic123";
            using (MailMessage mm = new MailMessage(From, To))
            {
                mm.Subject = "Cure Integrative Clinic";
                mm.Body = body;
                mm.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(From, Password);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
                return true;
            }
        }
        #endregion

    }
}